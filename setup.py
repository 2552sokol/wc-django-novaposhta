import os
import re
import setuptools


with open('README.md', 'r') as fh:
    long_description = fh.read()


def get_version(package):
    """
    Return package version as listed in `__version__` in `init.py`.
    """
    init_py = open(os.path.join(package, '__init__.py')).read()

    return re.search('__version__ = [\'"]([^\'"]+)[\'"]', init_py).group(1)


# version = get_version('wcd_user_checks')


setuptools.setup(
    name='wc-django-novaposhta',
    version=get_version('django_novaposhta'),
    author='WebCase',
    author_email='info@webcase.studio',
    license='MIT License',
    description='Nova poshta API integration.',
    install_requires=(
        'django-autocomplete-light>=3.9.0,<4.0',
        'django-awesome-standards @ git+https://gitlab.com/kastielspb/django-awesome-standards.git@a62f6a89f469cd64434e74b102cb6c613c0d5604',
    ),
    extras_require={},
    long_description=long_description,
    long_description_content_type='text/markdown',
    packages=setuptools.find_packages(exclude=(
        'tests', 'tests.*',
    )),
    python_requires='>=3.8',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',

        'Programming Language :: Python :: 3',

        'Intended Audience :: Developers',
        'Topic :: Utilities',

        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
    ],
    include_package_data=True
)

