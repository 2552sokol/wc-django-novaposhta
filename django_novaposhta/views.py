from dal import autocomplete

from .models import (
    Warehouse,
    Street,
    City,
)

__all__ = (
    'CityAutocomplete',
    'WarehousesAutocomplete',
    'StreetsAutocomplete',
)


class CityAutocomplete(autocomplete.Select2QuerySetView):
    model = City
    search_fields = ['description']

    def get_queryset(self):
        qs = super().get_queryset()
        area = self.forwarded.get('area', None)
        if area:
            qs = qs.filter(area=area)
        return qs


class CityFilterAutocompleteView(autocomplete.Select2QuerySetView):

    def get_queryset(self):
        qs = super().get_queryset()
        city = self.forwarded.get('city', None)
        if city:
            qs = qs.filter(city=city)
        return qs


class WarehousesAutocomplete(CityFilterAutocompleteView):
    model = Warehouse


class StreetsAutocomplete(CityFilterAutocompleteView):
    model = Street
