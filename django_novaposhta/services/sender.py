import time
from typing import Optional, Union

from .counterparty import (
    get_counterparties,
    get_counterparty_contact_persons,
)
from .addresses import create_counterparty_address
from ..models import (
    UserPreferences,
    Counterparty,
    Preferences,
    Address
)
from ..getters import (
    get_default_service_type_obj,
    get_sender_payer_type_obj,
)
from ..consts import (
    COUNTERPARTY_PRIVATE_PERSON_TYPE,
    WAREHOUSE_DEPARTURE_SERVICE_TYPES_MAP,
    DOORS_DEPARTURE_SERVICE_TYPES_MAP,
)


def update_with_sender_contact_person(
    *,
    sender_counterparty: 'Counterparty',
    preferences: Union['UserPreferences', 'Preferences', None] = None
) -> None:
    """
    Update user preferences with sender preferences data
    """
    if sender_counterparty.counterparty_type.ref == COUNTERPARTY_PRIVATE_PERSON_TYPE:

        contact_persons = get_counterparty_contact_persons(
            counterparty=sender_counterparty,
            preferences=preferences
        )
        sender_contact_person = contact_persons[0]
        preferences.sender_contact = sender_contact_person.ref
        preferences.first_name = sender_contact_person.first_name
        preferences.last_name = sender_contact_person.last_name
        preferences.middle_name = sender_contact_person.middle_name
        preferences.phone = sender_contact_person.phones
        preferences.sender_phone = sender_contact_person.phones

    else:
        """
        TODO: check Organization counterparty type:
         - get existed organisation counterparty contact persons
         - try create organisation counterparty contact persons
        """
        contact_persons = get_counterparty_contact_persons(
            counterparty=sender_counterparty,
            preferences=preferences
        )
        sender_contact_person = contact_persons[0]
        preferences.sender_contact = sender_contact_person.ref


def get_sender_address(
    preferences: Union['UserPreferences', 'Preferences'],
    counterparty: 'Counterparty',
) -> 'Address':
    return create_counterparty_address(
        counterparty=counterparty,
        street=preferences.street,
        building_number=preferences.building,
        flat=preferences.flat,
        preferences=preferences
    )


def populate_sender_preferences(
        sender_preferences: Union['UserPreferences', 'Preferences'],
        save: bool = False
):
    service_type = (
        sender_preferences.service_type or get_default_service_type_obj()
    )
    sender_counterparty = get_counterparties(
        counterparty_property=get_sender_payer_type_obj(),
        preferences=sender_preferences
    )[0]
    sender_counterparty.user = sender_preferences.user
    sender_preferences.save(update_fields=['user'])

    sender_preferences.sender = sender_counterparty.ref
    time.sleep(0.5)

    update_with_sender_contact_person(
        sender_counterparty=sender_counterparty,
        preferences=sender_preferences
    )
    time.sleep(0.5)

    if sender_preferences.city:
        sender_preferences.sender_city = sender_preferences.city.ref

    if service_type.ref in WAREHOUSE_DEPARTURE_SERVICE_TYPES_MAP.values():
        sender_preferences.sender_address = sender_preferences.warehouse.ref
    elif service_type.ref in DOORS_DEPARTURE_SERVICE_TYPES_MAP.values():
        address = get_sender_address(
            preferences=sender_preferences,
            counterparty=sender_counterparty
        )
        sender_preferences.sender_address = address.ref

    sender_preferences.is_active = True
    if save:
        sender_preferences.save()

    return sender_preferences
