from typing import TYPE_CHECKING, Optional, List, Union

from ..models import (
    Address as AddressModel,
    SettlementStreet as SettlementStreetModel,
    Street as StreetModel,
    Counterparty,
    Preferences,
    UserPreferences,
    Settlement,
)
from ..sdk import get_address_client

if TYPE_CHECKING:
    from ..entities.base import (
        CatalogItem, BaseImportEntity,

    )


def create_counterparty_address(
        *,
        counterparty: 'Counterparty',
        street: 'StreetModel',
        building_number: str,
        flat: str = '',
        note: str = '',
        preferences: Union['UserPreferences', 'Preferences', None] = None
) -> 'AddressModel':
    client = get_address_client(preferences=preferences)
    address_entity: 'CatalogItem' = client.save(
        counterparty_ref=counterparty.ref,
        street_ref=street.ref,
        building_number=building_number,
        flat=flat,
        note=note
    )

    data: 'Dict' = address_entity.as_dict()
    ref = data.pop('ref')
    address, _ = (
        AddressModel.objects.update_or_create(
            counterparty=counterparty,
            ref=ref,
            defaults=dict(
                street=street,
                **data
            )
        )
    )
    return address


def search_settlements_streets(
        *,
        settlement_ref: str,
        street_name: str,
        preferences: Union['Preferences', 'UserPreferences', None] = None
) -> List['SettlementStreetModel']:
    client = get_address_client(preferences=preferences)
    settlement_street_entities, _ = client.search_settlements_streets(
        settlement_ref=settlement_ref,
        street_name=street_name
    )
    street_objects = []
    for street_entity in settlement_street_entities:
        street_object, _ = SettlementStreetModel.objects.update_or_create(
            settlement_street_ref=street_entity.settlement_street_ref,
            defaults=street_entity.as_dict()
        )
        street_objects.append(street_object)
    return street_objects


def search_settlements(
        *,
        city_name: str,
        page: int = 1,
        limit: int = 50,
        preferences: Union['Preferences', 'UserPreferences', None] = None
) -> dict:
    client = get_address_client(preferences=preferences)
    settlement_entities, total_count = client.search_settlements(city_name=city_name)

    return {
        'settlements': list(
            Settlement
            .objects
            .filter(ref__in=[item.ref for item in settlement_entities])
            .select_related('area', 'region')
        ),
        'total_count': total_count,
        'page': page,
        'limit': limit
    }
