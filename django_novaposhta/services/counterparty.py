from typing import Dict, Iterator, List, TYPE_CHECKING, Optional, Union

from ..sdk import (
    get_counterparty_client,
    get_contact_person_client
)
from ..models import (
    Counterparty as CounterpartyModel,
    ContactPerson as ContactPersonModel,
    UserPreferences, Preferences
)

if TYPE_CHECKING:
    from django.contrib.auth import get_user_model
    UserModel = get_user_model()
    from ..entities import Counterparty, ContactPerson
    from ..models import CounterpartyType, PayerType


__all__ = (
    'create_counterparty',
    'update_counterparty',
    'create_counterparty_contact_person',
    'update_counterparty_contact_person',
    'get_counterparty_contact_persons',
    'get_counterparties'
)


def create_counterparty(
        *,
        first_name: str,
        middle_name: str,
        last_name: str,
        phone: str,
        email: str = '',
        city_ref: str = '',
        counterparty_type: 'CounterpartyType',
        counterparty_property: 'PayerType',
        user: Optional['UserModel'] = None,
        preferences: Union['Preferences', 'UserPreferences', None]
) -> 'CounterpartyModel':
    client = get_counterparty_client(preferences=preferences)

    counterparty_entity: 'Counterparty' = client.save(
        first_name=first_name,
        middle_name=middle_name,
        last_name=last_name,
        phone=phone,
        email=email,
        city_ref=city_ref,
        counterparty_type=counterparty_type.ref,
        counterparty_property=counterparty_property.ref
    )
    data: 'Dict' = counterparty_entity.as_dict()
    city = data.pop('city', None)
    data['city'] = city if city else None
    contact_person_data: 'Dict' = data.pop('contact_person')
    ref = data.pop('ref')
    counterparty, _ = (
        CounterpartyModel.objects.update_or_create(
            ref=ref,
            defaults=dict(
                user=user,
                phone=phone,
                email=email,
                counterparty_property=counterparty_property,
                **data
            )
        )
    )
    contact_person: 'ContactPersonModel' = (
        ContactPersonModel.objects.update_or_create(
            counterparty=counterparty,
            defaults=dict(**contact_person_data)
        )
    )

    return counterparty


def update_counterparty(
        *,
        counterparty: 'CounterpartyModel',
        first_name: str,
        middle_name: str,
        last_name: str,
        phone: str,
        email: str,
        city_ref: str = None,
        counterparty_type: 'CounterpartyType',
        counterparty_property: 'PayerType',
        user_preferences: Optional['UserPreferences'] = None
) -> 'CounterpartyModel':
    client = get_counterparty_client(preferences=user_preferences)

    counterparty_entity: 'Counterparty' = client.update(
        ref=counterparty.ref,
        first_name=first_name,
        middle_name=middle_name,
        last_name=last_name,
        phone=phone,
        email=email,
        city_ref=city_ref,
        counterparty_type=counterparty_type.ref,
        counterparty_property=counterparty_property.ref
    )

    data: 'Dict' = counterparty_entity.as_dict()
    # data.pop('city', None)
    ref = data.pop('ref')
    counterparty, updated = (
        CounterpartyModel.objects.update_or_create(
            ref=ref,
            defaults={
                **data,
                **{
                    'counterparty_property': counterparty_property,
                    'counterparty_type': counterparty_type,
                }
            }
        )
    )

    return counterparty


def create_counterparty_contact_person(
        *,
        counterparty: 'Counterparty',
        first_name: str,
        middle_name: str,
        last_name: str,
        phone: str,
        user_preferences: Optional['UserPreferences'] = None
) -> 'ContactPersonModel':
    client = get_contact_person_client(preferences=user_preferences)

    contact_person_entity: 'ContactPerson' = client.save(
        counterparty_ref=counterparty.ref,
        first_name=first_name,
        middle_name=middle_name,
        last_name=last_name,
        phone=phone
    )

    data: 'Dict' = contact_person_entity.as_dict()
    contact_person: 'ContactPersonModel' = (
        ContactPersonModel.objects.create(
            counterparty=counterparty,
            **data,
        )
    )

    return contact_person


def update_counterparty_contact_person(
        *,
        counterparty: 'Counterparty',
        contact_person: 'ContactPerson',
        first_name: str,
        middle_name: str,
        last_name: str,
        phone: str,
        email: str = None,
        user_preferences: Optional['UserPreferences'] = None
) -> 'ContactPersonModel':
    client = get_contact_person_client(preferences=user_preferences)

    contact_person_entity: 'ContactPerson' = client.update(
        ref=contact_person.ref,
        counterparty_ref=counterparty.ref,
        first_name=first_name,
        middle_name=middle_name,
        last_name=last_name,
        phone=phone,
        email=email,
    )

    data: 'Dict' = contact_person_entity.as_dict()
    ref = data.pop('ref')
    contact_person: 'ContactPersonModel' = (
        ContactPersonModel.objects.update_or_create(
            ref=ref,
            counterparty=counterparty,
            defaults=data,
        )
    )

    return contact_person


def get_counterparty_contact_persons(
        *,
        counterparty: 'Counterparty',
        preferences: Optional['UserPreferences'] = None
) -> List['ContactPersonModel']:
    client = get_counterparty_client(preferences=preferences)

    response: Iterator['ContactPerson'] = (
        client.get_contact_persons(
            counterparty_ref=counterparty.ref,
        )
    )

    contact_persons: List['ContactPersonModel'] = []

    for item in response:
        data: 'Dict' = item.as_dict()
        ref = data.pop('ref')
        contact_person, updated = (
            ContactPersonModel.objects.update_or_create(
                ref=ref,
                counterparty=counterparty,
                defaults=data
            )
        )
        contact_persons.append(contact_person)

    return contact_persons


def get_counterparties(
    counterparty_property: 'PayerType' = None,
    preferences: Union['UserPreferences', 'Preferences'] = None
) -> List['CounterpartyModel']:
    client = get_counterparty_client(preferences=preferences)
    response: Iterator['Counterparty'] = (
        client.get_counterparties(counterparty_property=counterparty_property.ref)
    )
    counterparties: List['CounterpartyModel'] = []

    for item in response:
        data: 'Dict' = item.as_dict()
        data.pop('contact_person')
        ref = data.pop('ref')
        # data.pop('city', None)

        counterparty, created = (
            CounterpartyModel.objects.update_or_create(
                ref=ref,
                defaults={
                    **data,
                    **{'counterparty_property': counterparty_property}
                }
            )
        )
        counterparties.append(counterparty)
    return counterparties
