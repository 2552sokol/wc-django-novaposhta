from dataclasses import dataclass
from decimal import Decimal
from typing import Dict, Any

from .base import BaseImportEntity
from ..consts import NOVAPOSHTA_WEIGHT_VOLUME_RATIO
from ..models import (
    City as CityModel,
    Area as AreaModel,
    Settlement as SettlementModel,
    SettlementType as SettlementTypeModel,
    Region as RegionModel,
    WarehouseType as WarehouseTypeModel
)

__all__ = (
    'Area',
    'City',
    'Settlement',
    'LiveSearchSettlement',
    'Warehouse',
    'Street',
    'SettlementStreet',
    'WarehouseLimitations',
)


@dataclass
class Area(BaseImportEntity):
    ref: str
    description: str
    areas_center: str
    raw_data: Dict = None

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        if self.areas_center:
            self.areas_center = (
                CityModel.objects
                .filter(ref=self.areas_center)
                .first()
            )


@dataclass
class City(BaseImportEntity):
    ref: str
    description: str
    description_ru: str
    area: str
    settlement_type: str
    settlement_type_description: str
    settlement_type_description_ru: str
    raw_data: Dict = None

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        if self.area:
            self.area = (
                AreaModel.objects.get(ref=self.area)
            )

        if self.settlement_type:
            self.settlement_type, _ = (
                SettlementTypeModel.objects.get_or_create(
                    ref=self.settlement_type,
                    defaults={
                        'description': self.settlement_type_description,
                        'description_ru': self.settlement_type_description_ru
                    }
                )
            )

    def as_dict(self):
        data = super().as_dict()
        data.pop('settlement_type_description', None)
        data.pop('settlement_type_description_ru', None)
        return data


@dataclass
class Settlement(BaseImportEntity):
    ref: str
    longitude: str
    latitude: str
    description: str
    description_ru: str
    settlement_type: str
    settlement_type_description: str
    settlement_type_description_ru: str
    region: str
    regions_description: str
    regions_description_ru: str
    area: str
    area_description: str
    area_description_ru: str
    raw_data: Dict = None

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        if self.area:
            self.area, _ = (
                AreaModel.objects
                .update_or_create(
                    ref=self.area,
                    is_active=False,
                    defaults={
                        'description': self.area_description,
                        'description_ru': self.area_description_ru,
                    }
                )
            )

        if self.settlement_type:
            self.settlement_type, _ = (
                SettlementTypeModel.objects.update_or_create(
                    ref=self.settlement_type,
                    defaults={
                        'description': self.settlement_type_description,
                        'description_ru': self.settlement_type_description_ru,
                    }
                )
            )

        if self.region:
            self.region, _ = (
                RegionModel.objects.update_or_create(
                    ref=self.region,
                    defaults={
                        'description': self.regions_description,
                        'description_ru': self.regions_description_ru,
                        'area': self.area
                    }
                )
            )
        else:
            self.region = None

    def as_dict(self):
        data = super().as_dict()
        data.pop('settlement_type_description', None)
        data.pop('settlement_type_description_ru', None)
        data.pop('regions_description', None)
        data.pop('regions_description_ru', None)
        data.pop('area_description', None)
        data.pop('area_description_ru', None)
        return data


@dataclass
class LiveSearchSettlement(BaseImportEntity):
    ref: str
    present: str
    warehouses: str
    main_description: str
    area: str
    region: str
    settlement_type_code: str
    delivery_city: str
    address_delivery_allowed: bool
    streets_availability: bool
    parent_region_types: str
    parent_region_code: str
    region_types: str
    region_types_code: str
    raw_data: Dict = None

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        if self.area:
            self.area = AreaModel.objects.filter(description=self.area).first()

        if self.region:
            regions = RegionModel.objects.filter(
                description=self.region,
                area__description=self.region
            )
            if regions.count() > 1:
                print(self.ref, self.present, regions)
            self.region = (
                RegionModel.objects.filter(
                    description=self.region,
                    area__description=self.region
                )
                .first()
            )
        else:
            self.region = None

    def as_dict(self):
        return {
            'ref': self.ref,
            'description': self.main_description,
            'area': self.area,
            'region': self.region,
        }


@dataclass
class Warehouse(BaseImportEntity):
    ref: str
    site_key: str
    description: str
    description_ru: str
    phone: str
    type_of_warehouse: str
    number: str
    city_ref: str
    longitude: str
    latitude: str
    post_finance: int
    POST_terminal: int
    reception: Dict
    delivery: Dict
    schedule: Dict
    city: Any = None
    raw_data: Dict = None

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        if self.type_of_warehouse:
            self.type_of_warehouse = (
                WarehouseTypeModel.objects
                .get(ref=self.type_of_warehouse)
            )

        if self.city_ref:
            try:
                self.city = (
                    CityModel.objects.get(ref=self.city_ref)
                )
            except CityModel.DoesNotExist:
                pass

    def as_dict(self):
        data = super().as_dict()
        data['post_terminal'] = data.pop('POST_terminal', None)
        return data


@dataclass
class Street(BaseImportEntity):
    ref: str
    description: str
    streets_type: str
    streets_type_ref: str
    city_ref: str
    city: Any = None
    raw_data: Dict = None

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        if self.city_ref:
            try:
                self.city = (
                    CityModel.objects.get(ref=self.city_ref)
                )
            except CityModel.DoesNotExist:
                pass

    def as_dict(self):
        data = super().as_dict()
        data.pop('city_ref', None)
        return data


@dataclass
class SettlementStreet(BaseImportEntity):
    settlement_ref: str
    settlement_street_ref: str
    settlement_street_description: str
    present: str
    streets_type: str
    streets_type_description: str
    location: Dict
    settlement: Any = None
    settlement_street_description_ru = str
    raw_data: Dict = None

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    @property
    def ref(self):
        return self.settlement_street_ref

    def as_dict(self):
        data = super().as_dict()
        settlement_ref = data.pop('settlement_ref', None)
        if settlement_ref:
            data['settlement'] = (
                SettlementModel.objects.filter(ref=settlement_ref).first()
            )
        return data


@dataclass
class WarehouseLimitations:
    total_weight: Decimal
    width: Decimal
    height: Decimal
    length: Decimal

    def validate_dimensions(self, option_seat: dict):
        volume_weight = option_seat['volumetricVolume'] * NOVAPOSHTA_WEIGHT_VOLUME_RATIO
        over_weight = (
            option_seat['weight'] > self.total_weight or
            volume_weight > self.total_weight
        )
        over_size = (
            option_seat['volumetricLength'] > self.length or
            option_seat['volumetricHeight'] > self.height or
            option_seat['volumetricWidth'] > self.width
        )

        return not (over_weight or over_size)

