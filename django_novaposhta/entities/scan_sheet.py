from dataclasses import dataclass
from typing import Any, Dict

from .base import BaseImportEntity
from ..models import City as CityModel

__all__ = (
    'ScanSheet',
)


@dataclass
class ScanSheet(BaseImportEntity):
    ref: str
    number: str
    date: str
    printed: int = None
    count: int = None
    city_sender_ref: str = None
    city_sender: Any = None
    sender_address: str = None
    sender: str = None
    raw_data: Dict = None

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        if self.city_sender_ref:
            self.city_sender = (
                CityModel.objects.get(ref=self.city_sender_ref)
            )

    def as_dict(self):
        data = super().as_dict()
        data.pop('city_sender_ref', None)
        return data
