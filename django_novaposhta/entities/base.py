from dataclasses import asdict, dataclass
from typing import Dict

from ..utils import undo_snake_case_key

__all__ = (
    'BaseImportEntity',
    'CatalogItem'
)


class BaseImportEntity:
    def __init__(self, *args, **kwargs):
        for field_name, field_type in self.__annotations__.items():
            api_field = undo_snake_case_key(field_name)

            if field_type is str:
                default = ''
            elif field_type is int:
                default = 0
            elif field_type is Dict:
                default = {}
            else:
                default = None

            if api_field in kwargs:
                setattr(self, field_name, kwargs.get(api_field) or default)
            elif field_name == 'raw_data':
                setattr(self, field_name, kwargs)
            else:
                setattr(self, field_name, default)

    def as_dict(self):
        data = asdict(self)
        return data


@dataclass
class CatalogItem(BaseImportEntity):
    ref: str
    description: str = None
    description_ru: str = None
    raw_data: Dict = None

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
