from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class DjangoNovaposhtaConfig(AppConfig):
    name = 'django_novaposhta'
    verbose_name = _('Novaposhta post')
