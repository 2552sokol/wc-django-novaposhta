from django.dispatch import Signal

__all__ = (
    'api_request_signal',
)


api_request_signal = Signal(providing_args=[
    "source",
    "method",
    "status_code",
    "request_data",
    "response",
    "user_id"
])
