from typing import Type, TYPE_CHECKING, Union

from .base import BaseServiceAdapter
from ..client import SyncRequestClient
from ..entities import ContactPerson, CatalogItem
from ..models import Preferences, UserPreferences

if TYPE_CHECKING:
    from ..client import BaseRequestClient

__all__ = (
    'ContactPersonService',
    'get_contact_person_client'
)


class ContactPersonService(BaseServiceAdapter):
    MODEL_NAME = 'ContactPerson'

    def save(
            self,
            counterparty_ref: str,
            first_name: str,
            middle_name: str,
            last_name: str,
            phone: str,
    ) -> 'ContactPerson':
        response: 'ContactPerson' = self.make_request(
            called_method="save",
            entity_class=ContactPerson,
            method_properties={
                'CounterpartyRef': counterparty_ref,
                'FirstName': first_name,
                'MiddleName': middle_name,
                'LastName': last_name,
                'Phone': phone,
            },
            is_single=True
        )

        return response

    def update(
            self,
            ref: str,
            counterparty_ref: str,
            first_name: str,
            middle_name: str,
            last_name: str,
            phone: str,
            email: str = None
    ) -> 'ContactPerson':
        response: 'ContactPerson' = self.make_request(
            called_method="update",
            entity_class=ContactPerson,
            method_properties={
                'Ref': ref,
                'CounterpartyRef': counterparty_ref,
                'FirstName': first_name,
                'MiddleName': middle_name,
                'LastName': last_name,
                'Phone': phone,
                'Email': email
            },
            is_single=True
        )

        return response

    def delete(self, ref: str) -> 'CatalogItem':
        response: 'CatalogItem' = self.make_request(
            called_method="delete",
            entity_class=CatalogItem,
            method_properties={
                'Ref': ref,
            },
            is_single=True
        )

        return response


def get_contact_person_client(
        request_client_class: Type['BaseRequestClient'] = SyncRequestClient,
        preferences: Union['UserPreferences', 'Preferences', None] = None
) -> 'ContactPersonService':
    preferences = preferences or Preferences.get_solo()

    client = ContactPersonService(
        api_key=preferences.api_key,
        request_client_class=request_client_class
    )

    return client
