from typing import Dict, Iterator, List, Type, TYPE_CHECKING, Union

from .base import BaseServiceAdapter
from ..client import SyncRequestClient
from ..entities import CatalogItem, ScanSheet
from ..exceptions import NovaposhtaResponseError
from ..models import Preferences, UserPreferences

if TYPE_CHECKING:
    from ..client import BaseRequestClient

__all__ = (
    'ScanSheetService',
    'get_scan_sheet_client'
)


class ScanSheetService(BaseServiceAdapter):
    MODEL_NAME = 'ScanSheet'

    def check_errors(self, response_json: Dict):
        errors = response_json.get("errors")

        data = response_json.get('data')

        if isinstance(data, list):
            for item in data:
                errors = (
                    item.get('Data', {}).get('Errors', [])
                    or item.get('Errors', [])
                )

                if errors:
                    raise NovaposhtaResponseError(errors)

        if isinstance(data, dict):
            for item in data.values():
                errors = item.get('Errors', [])

        if errors:
            raise NovaposhtaResponseError(errors)

    def insert_documents(
            self, document_refs: List[str]
    ) -> 'ScanSheet':
        response: 'ScanSheet' = self.make_request(
            called_method="insertDocuments",
            entity_class=ScanSheet,
            method_properties={
                "DocumentRefs": document_refs
            },
            is_single=True
        )
        if not response.ref:
            raise NovaposhtaResponseError('Not created')
        return response

    def expand(
            self,
            document_refs: List[str],
            scan_sheet_ref: str
    ) -> 'ScanSheet':
        response: 'ScanSheet' = self.make_request(
            called_method="insertDocuments",
            entity_class=ScanSheet,
            method_properties={
                "DocumentRefs": document_refs,
                "Ref": scan_sheet_ref
            },
            is_single=True
        )

        return response

    def get(self, ref: str) -> 'ScanSheet':
        response: 'ScanSheet' = self.make_request(
            called_method="getScanSheet",
            entity_class=ScanSheet,
            method_properties={
                "Ref": ref
            },
            is_single=True
        )
        return response

    def get_list(self) -> Iterator['ScanSheet']:
        response: Iterator['ScanSheet'] = self.make_request(
            called_method="getScanSheetList",
            entity_class=ScanSheet
        )

        return response

    def delete(self, refs: List[str]) -> List[dict]:
        response: List = self.make_request(
            called_method="deleteScanSheet",
            method_properties={"ScanSheetRefs": refs}
        )

        return response

    def remove_documents(
            self, document_refs: List[str]
    ) -> Iterator['CatalogItem']:
        response: Iterator['CatalogItem'] = self.make_request(
            called_method="removeDocuments",
            method_properties={"DocumentRefs": document_refs}
        )

        return response


def get_scan_sheet_client(
        request_client_class: Type['BaseRequestClient'] = SyncRequestClient,
        preferences: Union['UserPreferences', 'Preferences', None] = None
) -> 'ScanSheetService':
    preferences = preferences or Preferences.objects.first()
    client = ScanSheetService(
        api_key=preferences.api_key,
        request_client_class=request_client_class
    )

    return client
