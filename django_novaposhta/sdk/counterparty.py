from typing import Iterator, List, Type, TYPE_CHECKING, Union

from .base import BaseServiceAdapter
from ..client import SyncRequestClient
from ..entities import (
    ContactPerson,
    Counterparty,
    CatalogItem
)
from ..models import Preferences, UserPreferences

if TYPE_CHECKING:
    from ..client import BaseRequestClient

__all__ = (
    'CounterpartyService',
    'get_counterparty_client'
)


class CounterpartyService(BaseServiceAdapter):
    MODEL_NAME = 'Counterparty'

    def save(
            self,
            first_name: str,
            middle_name: str,
            last_name: str,
            phone: str,
            email: str,
            city_ref: str,
            counterparty_type: str = 'PrivatePerson',
            counterparty_property: str = 'Recipient'
    ) -> 'Counterparty':
        response: 'Counterparty' = self.make_request(
            called_method="save",
            method_properties={
                'FirstName': first_name,
                'MiddleName': middle_name,
                'LastName': last_name,
                'Phone': phone,
                'Email': email,
                'CityRef': city_ref,
                'CounterpartyType': counterparty_type,
                'CounterpartyProperty': counterparty_property
            },
            entity_class=Counterparty,
            is_single=True
        )

        return response

    def update(
            self,
            ref: str,
            first_name: str,
            middle_name: str,
            last_name: str,
            phone: str,
            email: str,
            city_ref: str,
            counterparty_type: str = 'PrivatePerson',
            counterparty_property: str = 'Recipient'
    ) -> 'Counterparty':
        response: 'Counterparty' = self.make_request(
            called_method="update",
            method_properties={
                'Ref': ref,
                'FirstName': first_name,
                'MiddleName': middle_name,
                'LastName': last_name,
                'Phone': phone,
                'Email': email,
                'CityRef': city_ref,
                'CounterpartyType': counterparty_type,
                'CounterpartyProperty': counterparty_property
            },
            entity_class=Counterparty,
            is_single=True
        )

        return response

    def delete(self, ref: str) -> 'CatalogItem':
        response: 'CatalogItem' = self.make_request(
            called_method="delete",
            entity_class=CatalogItem,
            method_properties={
                'Ref': ref,
            },
            is_single=True
        )

        return response

    def get_addresses(
            self,
            counterparty_ref: str,
            counterparty_property: str = 'Recipient'
    ) -> Iterator['CatalogItem']:
        response: 'List' = self.make_request(
            called_method="getCounterpartyAddresses",
            entity_class=CatalogItem,
            method_properties={
                'Ref': counterparty_ref,
                'CounterpartyProperty': counterparty_property,
            }
        )

        return response

    def get_contact_persons(
            self,
            counterparty_ref: str,
            page: int = 1
    ) -> Iterator['ContactPerson']:
        response: Iterator['ContactPerson'] = self.make_request(
            called_method="getCounterpartyContactPersons",
            entity_class=ContactPerson,
            method_properties={
                'Ref': counterparty_ref,
            },
            page=page
        )

        return response

    def get_counterparties(
            self,
            counterparty_property: str = 'Sender',
            page: int = 1
    ) -> Iterator['Counterparty']:
        """
        Загрузить список Контрагентов отправителей/получателей/третье лицо
        :param counterparty_property: PayerType ref
        :param page: page number
        """
        response: Iterator['Counterparty'] = self.make_request(
            called_method="getCounterparties",
            entity_class=Counterparty,
            method_properties={
                'CounterpartyProperty': counterparty_property,
            },
            page=page
        )
        return response


def get_counterparty_client(
        *,
        request_client_class: Type['BaseRequestClient'] = SyncRequestClient,
        preferences: Union['UserPreferences', 'Preferences', None] = None
) -> 'CounterpartyService':
    preferences = preferences or Preferences.get_solo()
    client = CounterpartyService(
        api_key=preferences.api_key,
        request_client_class=request_client_class
    )
    return client
