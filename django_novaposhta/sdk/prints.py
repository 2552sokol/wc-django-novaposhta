from typing import List, Type, TYPE_CHECKING, Union

from .base import BaseServiceAdapter
from ..client import SyncRequestClient
from ..consts import (
    PRINT_DOCUMENT_URL,
    PRINT_DOCUMENTS_URL,
    PRINT_MARKING_URL,
    PRINT_MARKINGS_URL,
    PRINT_SCAN_SHEET_URL
)
from ..models import Preferences, UserPreferences

if TYPE_CHECKING:
    from ..client import BaseRequestClient

__all__ = (
    'PrintService',
    'get_prints_client'
)


class PrintService(BaseServiceAdapter):
    def get_document_url(
            self, number: str, type_: str = 'pdf'
    ) -> str:
        return (
            PRINT_DOCUMENT_URL.format(
                number=number,
                type=type_,
                api_key=self.api_key
            )
        )

    def get_documents_url(
            self, numbers: List[str], type_: str = 'pdf'
    ) -> str:
        numbers_string = ','.join(numbers)
        return (
            PRINT_DOCUMENTS_URL.format(
                numbers=numbers_string,
                type=type_,
                api_key=self.api_key
            )
        )

    def get_marking_url(
            self,
            number: str,
            size: str = '85x85',
            type_: str = 'pdf8',
            is_zebra_printer: bool = False
    ) -> str:
        if is_zebra_printer:
            type_ = 'pdf'
            size = '100x100'

        url = (
            PRINT_MARKING_URL.format(
                number=number,
                size=size,
                type=type_,
                api_key=self.api_key
            )
        )

        if is_zebra_printer:
            url += '/zebra'

        return url

    def get_markings_url(
            self,
            numbers: List[str],
            size: str = '85x85',
            type_: str = 'pdf8',
            is_zebra_printer: bool = False
    ) -> str:
        numbers_string = ','.join(numbers)

        if is_zebra_printer:
            type_ = 'pdf/'
            # type_ += '/'.join(list(repeat('zebra', len(numbers))))
            type_ += 'zebra/zebra/'
            size = '100x100'

        url = (
            PRINT_MARKINGS_URL.format(
                numbers=numbers_string,
                size=size,
                type=type_,
                api_key=self.api_key
            )
        )

        return url

    def get_scan_sheet_url(
            self, ref: str, type_: str = 'pdf'
    ) -> str:
        return (
            PRINT_SCAN_SHEET_URL.format(
                ref=ref,
                type=type_,
                api_key=self.api_key
            )
        )


def get_prints_client(
        request_client_class: Type['BaseRequestClient'] = SyncRequestClient,
        preferences: Union['Preferences', 'UserPreferences', None] = None
) -> 'PrintService':
    preferences = preferences or Preferences.get_solo()
    client = PrintService(
        api_key=preferences.api_key,
        request_client_class=request_client_class
    )

    return client
