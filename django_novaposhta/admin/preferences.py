from django.contrib import admin
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.utils.html import mark_safe
from django.utils.translation import pgettext_lazy

from solo.admin import SingletonModelAdmin

from .forms import PreferencesAdminForm, UserPreferencesAdminForm
from ..exceptions import NovaposhtaException
from ..getters import db_is_populated
from ..models import (
    Preferences,
    UserPreferences,
)
from ..services import populate_sender_preferences
from ..tasks import (
    update_novaposhta_task,
    update_warehouses_settlement_refs
)


class PopulateSenderPreferencesMixin:

    def populate_sender_preferences(self, obj):
        msg = pgettext_lazy("novaposhta", "Populate")
        if not db_is_populated():
            return '-'
        return mark_safe(
            f'<input type="submit" '
            f'value={msg} '
            f'name="_populate_sender_preferences" />'
        )

    populate_sender_preferences.short_description = (
        pgettext_lazy("novaposhta", "Populate sender preferences")
    )

    def response_change(self, request, obj):
        if "_populate_sender_preferences" in request.POST:
            if obj:
                try:
                    populate_sender_preferences(obj, save=True)
                    self.message_user(
                        request,
                        pgettext_lazy(
                            "novaposhta", 'Success. Sender preference has been populated'
                        ),
                        level=messages.SUCCESS,
                    )
                    return HttpResponseRedirect(".")

                except NovaposhtaException:
                    obj.is_active = False
                    obj.save(update_fields=('is_active',))
                    self.message_user(
                        request,
                        pgettext_lazy(
                            "novaposhta", 'Something went wrong. '
                                          'Please, check entered data or try a bit later.'),
                        level=messages.ERROR,
                    )
                    return HttpResponseRedirect(".")
        return super().response_change(request, obj)


@admin.register(Preferences)
class PreferencesAdmin(
    PopulateSenderPreferencesMixin,
    SingletonModelAdmin
):
    form = PreferencesAdminForm
    fieldsets = [
        (pgettext_lazy("novaposhta", "API info"), {
            'fields': [
                'api_key',
                'api_url',
                'update_novaposhta',
                'populate_warehouses_with_settlement_ref',
                'populate_sender_preferences',
            ]
        }),
        (pgettext_lazy("novaposhta", "Directory default values"), {
            'fields': (
                'payer_type',
                'payment_form',
                'cargo_type',
                'service_type',
                'cost',
                'weight',
                'seats_amount',
                'description',
            ),
        }),
        (pgettext_lazy("novaposhta", "Default Address"), {
            'fields': [
                'city',
                'street',
                'building',
                'flat',
                'warehouse',
            ]
        }),
        (pgettext_lazy("novaposhta", "Invoice default info"), {
            'fields': (
                'first_name',
                'last_name',
                'middle_name',
                'email',
                'phone',
                'sender',
                'sender_city',
                'sender_address',
                'sender_contact',
                'sender_phone',
            ),
        }),
    ]
    readonly_fields = (
        'update_novaposhta',
        'populate_warehouses_with_settlement_ref',
        'populate_sender_preferences',
    )

    def update_novaposhta(self, obj):
        msg = pgettext_lazy("novaposhta", "Run")

        return mark_safe(
            f'<input type="submit" '
            f'value={msg} '
            f'name="_update_novaposhta" />'
        )

    update_novaposhta.short_description = (
        pgettext_lazy("novaposhta", "Update novaposhta")
    )

    def populate_warehouses_with_settlement_ref(self, obj):
        msg = pgettext_lazy("novaposhta", "Populate")
        if not db_is_populated():
            return '-'
        return mark_safe(
            f'<input type="submit" '
            f'value={msg} '
            f'name="_populate_warehouses_with_settlement_ref" />'
        )

    populate_warehouses_with_settlement_ref.short_description = (
        pgettext_lazy("novaposhta", "Populate warehouses with settlement ref")
    )

    def response_change(self, request, obj):
        if "_update_novaposhta" in request.POST:
            update_novaposhta_task.delay()
            self.message_user(
                request,
                pgettext_lazy(
                    "novaposhta",
                    '`update_novaposhta` task run'
                ),
            )
            return HttpResponseRedirect(".")
        elif "_populate_warehouses_with_settlement_ref" in request.POST:
            update_warehouses_settlement_refs.delay()
            self.message_user(
                request,
                pgettext_lazy(
                    "novaposhta",
                    '`populate_warehouses_with_settlement_ref` task run'
                ),
            )
            return HttpResponseRedirect(".")
        return super().response_change(request, obj)


@admin.register(UserPreferences)
class UserPreferencesAdmin(
    PopulateSenderPreferencesMixin,
    admin.ModelAdmin
):
    form = UserPreferencesAdminForm
    fieldsets = [
        (pgettext_lazy("novaposhta", "General"), {
            'fields': [
                'api_key',
                'user',
                'is_active',
                'payer_type',
                'payment_form',
                'cargo_type',
                'service_type',
                'created_at',
                'updated_at',
                'populate_sender_preferences',
            ]
        }),
        (pgettext_lazy("novaposhta", "Address"), {
            'fields': [
                'city',
                'street',
                'building',
                'flat',
                'warehouse',
            ]
        }),
        (pgettext_lazy("novaposhta", "Invoice default info"), {
            'fields': (
                'first_name',
                'last_name',
                'middle_name',
                'email',
                'phone',
                'sender',
                'sender_city',
                'sender_address',
                'sender_contact',
                'sender_phone',
            ),
        }),
    ]
    list_display = (
        'id',
        'user',
        'is_active',
        'created_at',
        'updated_at'
    )
    readonly_fields = (
        'created_at',
        'updated_at',
        'populate_sender_preferences',
    )
    list_display_links = ('id', 'user')
    search_fields = ('user',)

 