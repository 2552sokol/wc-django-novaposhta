from django.contrib import admin

from ..models import (
    BackwardDeliveryCargoType,
    CargoType,
    CounterpartyType,
    PayerType,
    PaymentForm,
    ServiceType,
    SettlementType,
    WarehouseType,
)


@admin.register(WarehouseType)
class WarehouseTypeAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "ref",
        "description"
    )
    readonly_fields = (
        'created_at',
        'updated_at'
    )
    search_fields = (
        "ref",
        "description",
    )


@admin.register(PayerType)
class PayerTypeAdmin(WarehouseTypeAdmin):
    pass


@admin.register(PaymentForm)
class PaymentFormAdmin(WarehouseTypeAdmin):
    pass


@admin.register(CargoType)
class CargoTypeAdmin(WarehouseTypeAdmin):
    pass


@admin.register(BackwardDeliveryCargoType)
class BackwardDeliveryCargoTypeAdmin(WarehouseTypeAdmin):
    pass


@admin.register(ServiceType)
class ServiceTypeAdmin(WarehouseTypeAdmin):
    pass


@admin.register(CounterpartyType)
class CounterpartyTypeAdmin(WarehouseTypeAdmin):
    pass


@admin.register(SettlementType)
class SettlementTypeAdmin(WarehouseTypeAdmin):
    pass
