from django import forms
from dal import autocomplete

from ..exceptions import NovaposhtaValidationError
from ..models import (
    Address,
    Preferences,
    UserPreferences,
    Warehouse,
    Street,
    City,
)
from ..validators import validate_preferences_service_type

__all__ = (
    'CityAutocompleteFormMixin',
    'StreetAutocompleteFormMixin',
    'WarehouseAutocompleteFormMixin',
    'AddressAdminForm',
    'PreferencesAdminForm',
    'UserPreferencesAdminForm',
)


class AddressAdminForm(forms.ModelForm):
    street = forms.ModelChoiceField(
        queryset=Street.objects.all(),
        widget=autocomplete.ModelSelect2(
            url='novaposhta:autocomplete:streets',
        ),
        required=False
    )

    class Meta:
        model = Address
        fields = '__all__'


class CityAutocompleteFormMixin(forms.Form):
    city = forms.ModelChoiceField(
        queryset=City.objects.all(),
        widget=autocomplete.ModelSelect2(
            url='novaposhta:autocomplete:cities',
        ),
        required=False
    )


class StreetAutocompleteFormMixin(forms.Form):
    street = forms.ModelChoiceField(
        queryset=Street.objects.all(),
        widget=autocomplete.ModelSelect2(
            url='novaposhta:autocomplete:streets',
            forward=['city']
        ),
        required=False
    )


class WarehouseAutocompleteFormMixin(forms.Form):
    warehouse = forms.ModelChoiceField(
        queryset=Warehouse.objects.all(),
        widget=autocomplete.ModelSelect2(
            url='novaposhta:autocomplete:warehouses',
            forward=['city']
        ),
        required=False
    )


class PreferencesAdminFormMixin(
    WarehouseAutocompleteFormMixin,
    StreetAutocompleteFormMixin,
    forms.Form
):

    def clean(self):
        cleaned_data = super().clean()
        try:
            validate_preferences_service_type(cleaned_data, self.instance)
        except NovaposhtaValidationError as e:
            raise forms.ValidationError(str(e))
        return cleaned_data


class PreferencesAdminForm(
    PreferencesAdminFormMixin,
    forms.ModelForm
):

    class Meta:
        model = Preferences
        fields = '__all__'


class UserPreferencesAdminForm(
    PreferencesAdminFormMixin,
    forms.ModelForm
):

    class Meta:
        model = UserPreferences
        fields = '__all__'
