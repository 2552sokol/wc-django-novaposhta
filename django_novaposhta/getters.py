from typing import TYPE_CHECKING, Optional

from django.utils.translation import pgettext_lazy

from .consts import (
    RECIPIENT_REF,
    DEFAULT_PAYER_TYPE,
    DEFAULT_SERVICE_TYPE,
    DEFAULT_PAYMENT_METHOD,
    DEFAULT_COUNTERPARTY_TYPE,
    DEFAULT_CARGO_TYPE,
)
from .exceptions import NovaposhtaException
from .models import (
    ServiceType,
    PaymentForm,
    PayerType,
    CounterpartyType,
    CargoType,
    UserPreferences,
    Preferences,
    Warehouse,
    City,
    Street
)

if TYPE_CHECKING:
    from django.conf import settings
    UserModel = settings.AUTH_USER_MODEL

__all__ = (
    'get_default_payment_form_obj',
    'get_service_type_obj',
    'get_default_service_type_obj',
    'get_payer_type_obj',
    'get_sender_payer_type_obj',
    'get_default_payer_type_obj',
    'get_privat_person_counterparty_type_obj',

    'get_default_cargo_type_obj',

    'get_preferences',
    'get_user_preferences',
    'active_user_preferences_receiver',
    'db_is_populated',
)


# PayerType

SENDER_REF = DEFAULT_PAYER_TYPE


def get_payer_type_obj(ref: str) -> PayerType:
    return PayerType.objects.get(ref=ref)


def get_sender_payer_type_obj() -> PayerType:
    return get_payer_type_obj(ref=SENDER_REF)


def get_default_payer_type_obj() -> PayerType:
    return get_payer_type_obj(ref=RECIPIENT_REF)

# PaymentForm


def get_payment_form_obj(ref: str) -> PaymentForm:
    return PaymentForm.objects.get(ref=ref)


def get_default_payment_form_obj() -> PaymentForm:
    return get_payment_form_obj(ref=DEFAULT_PAYMENT_METHOD)

# ServiceType


def get_service_type_obj(ref: str) -> ServiceType:
    return ServiceType.objects.get(ref=ref)


def get_default_service_type_obj() -> ServiceType:
    return get_service_type_obj(ref=DEFAULT_SERVICE_TYPE)

# CounterpartyType


def get_counterparty_type_obj(ref: str) -> CounterpartyType:
    return CounterpartyType.objects.get(ref=ref)


def get_privat_person_counterparty_type_obj() -> CounterpartyType:
    return get_counterparty_type_obj(ref=DEFAULT_COUNTERPARTY_TYPE)

# CargoType


def get_cargo_type_obj(ref: str) -> CargoType:
    return CargoType.objects.get(ref=ref)


def get_default_cargo_type_obj() -> CargoType:
    return get_cargo_type_obj(ref=DEFAULT_CARGO_TYPE)


def get_preferences():
    return Preferences.get_solo()


def get_user_preferences(user: 'UserModel') -> Optional['UserPreferences']:
    return UserPreferences.objects.filter(user=user).first()


def active_user_preferences_receiver(user: 'UserModel' = None):
    try:
        return UserPreferences.objects.get(user=user, is_active=True)
    except UserPreferences.DoesNotExist:
        raise NovaposhtaException(
            pgettext_lazy(
                'django_novaposhta', 'Preferences is not active or does not exists'
            )
        )


def db_is_populated():
    return (
        City.objects.exists() and
        Street.objects.exists() and
        Warehouse.objects.exists()
    )
