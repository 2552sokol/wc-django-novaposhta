from typing import Dict
from urllib.request import urlopen

from django.core.files import File
from django.core.files.temp import NamedTemporaryFile
from django.utils import timezone
from django.utils.text import slugify

from django_novaposhta.exceptions import NovaposhtaResponseError

__all__ = (
    'clean_dict',
    'undo_snake_case_key',
    'create_temporary_file_from_url',
    'file_upload_to'
)

PRINTS_CONTENT_TYPE = 'application/pdf'


def clean_dict(data: Dict) -> Dict:
    if data is None:
        return {}

    return {
        key: value for key, value in data.items()
        if value is not None
    }


def undo_snake_case_key(key: str) -> str:
    new_key = key.split('__')
    int_key = '-'.join(new_key)
    new_key = int_key.split('_')
    return ''.join([it.title() for it in new_key])


def create_temporary_file_from_url(url: str):
    resource = urlopen(url)
    content_type = resource.info().get_content_type()
    if content_type != PRINTS_CONTENT_TYPE:
        raise NovaposhtaResponseError('Is not available')
    file_temp = NamedTemporaryFile(delete=True)
    file_temp.write(resource.read())
    file_temp.flush()

    return File(file_temp)


def file_upload_to(instance, filename):
    name, ext = filename.rsplit('.', 1)
    filename = f'{slugify(name.lower())}.{ext}'
    tz_now = (
        timezone
        .localtime(timezone.now())
        .strftime('%Y/%m/%d')
    )

    if hasattr(instance, 'content_object'):
        class_name = instance.content_object.__class__.__name__
    else:
        class_name = instance.__class__.__name__

    return (
        f"{instance._meta.app_label}/"
        f"{slugify(class_name)}/"
        f"{tz_now}/"
        f"{filename}"
    )
