from django.conf import settings
from django.core.files.storage import FileSystemStorage

__all__ = (
    'novaposhta_upload_storage',
)

NOVAPOSHTA_MEDIA_ROOT = getattr(settings, 'NOVAPOSHTA_MEDIA_ROOT', settings.MEDIA_ROOT)
NOVAPOSHTA_MEDIA_URL = getattr(settings, 'NOVAPOSHTA_MEDIA_URL', settings.MEDIA_URL)

novaposhta_upload_storage = FileSystemStorage(
    location=str(NOVAPOSHTA_MEDIA_ROOT),
    base_url=str(NOVAPOSHTA_MEDIA_URL)
)
