from functools import wraps
import re

from rest_framework.exceptions import ValidationError

from ..exceptions import NovaposhtaResponseError

__all__ = (
    'handle_novaposhta_exception',
)


NOVAPOSHTA_ERROR_CODE = 'novaposhta_response_error'


def handle_novaposhta_exception(func):
    @wraps(func)
    def decorator(request, *args, **kwargs):
        try:
            return func(request, *args, **kwargs)
        except NovaposhtaResponseError as e:
            error_msg = re.sub(
                r'\[|\]', '', str(e)
            )

            error_dict = {
                'non_field_errors': error_msg
            }

            raise ValidationError(
                error_dict,
                code=NOVAPOSHTA_ERROR_CODE
            )
    return decorator
