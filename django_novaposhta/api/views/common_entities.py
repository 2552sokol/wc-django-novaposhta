from rest_framework import filters
from rest_framework.generics import ListAPIView, RetrieveAPIView
from rest_framework.permissions import AllowAny
from django_filters.rest_framework import DjangoFilterBackend
from standards.drf.views import StandardAPIViewMixin
from standards.drf.pagination import LimitOffsetPagination

from ..filters import (
    OrderingSearchFilter,
    StreetsFilterSet,
)
from ..serializers import (
    AreaSerializer,
    CargoTypeSerializer,
    CitySerializer,
    CounterpartyTypeSerializer,
    PayerTypeSerializer,
    PaymentFormSerializer,
    RegionSerializer,
    ServiceTypeSerializer,
    SettlementTypeSerializer,
    SettlementSerializer,
    StreetSerializer,
    WarehouseTypeSerializer,
    WarehouseSerializer,
    WarehouseDetailSerializer,
)
from ...models import (
    Area,
    CargoType,
    City,
    CounterpartyType,
    PayerType,
    PaymentForm,
    Region,
    ServiceType,
    Settlement,
    SettlementType,
    Street,
    WarehouseType,
    Warehouse,

)

__all__ = (
    "BaseNovaposhtaListAPIView",
    "CargoTypeListAPIView",
    "CounterpartyTypeListAPIView",
    "PayerTypeListAPIView",
    "PaymentFormListAPIView",
    "ServiceTypeListAPIView",
    "WarehouseTypeListAPIView",
    "AreaListAPIView",
    "RegionListAPIView",
    "SettlementTypeListAPIView",
    "SettlementListAPIView",
    "RegionListAPIView",
    "CityListAPIView",
    "StreetListAPIView",
    "WarehouseListAPIView",
    "WarehouseDetailAPIView",
)


class BaseNovaposhtaListAPIView(StandardAPIViewMixin, ListAPIView):
    filter_backends = (
        OrderingSearchFilter,
        filters.OrderingFilter,
        DjangoFilterBackend
    )
    search_fields = ("description", )
    pagination_class = LimitOffsetPagination
    pagination_class.default_limit = 25
    permission_classes = (AllowAny, )


class CargoTypeListAPIView(BaseNovaposhtaListAPIView):
    queryset = CargoType.objects.all()
    serializer_class = CargoTypeSerializer


class CounterpartyTypeListAPIView(BaseNovaposhtaListAPIView):
    queryset = CounterpartyType.objects.all()
    serializer_class = CounterpartyTypeSerializer


class PayerTypeListAPIView(BaseNovaposhtaListAPIView):
    queryset = PayerType.objects.all()
    serializer_class = PayerTypeSerializer


class PaymentFormListAPIView(BaseNovaposhtaListAPIView):
    queryset = PaymentForm.objects.all()
    serializer_class = PaymentFormSerializer


class ServiceTypeListAPIView(BaseNovaposhtaListAPIView):
    queryset = ServiceType.objects.all()
    serializer_class = ServiceTypeSerializer


class WarehouseTypeListAPIView(BaseNovaposhtaListAPIView):
    queryset = WarehouseType.objects.all()
    search_fields = ("description", "description_ru")
    serializer_class = WarehouseTypeSerializer


class AreaListAPIView(BaseNovaposhtaListAPIView):
    queryset = Area.objects.filter(is_active=True)
    serializer_class = AreaSerializer


class RegionListAPIView(BaseNovaposhtaListAPIView):
    queryset = Region.objects.all()
    serializer_class = RegionSerializer


class SettlementTypeListAPIView(BaseNovaposhtaListAPIView):
    queryset = SettlementType.objects.all()
    serializer_class = SettlementTypeSerializer


class SettlementListAPIView(BaseNovaposhtaListAPIView):
    filterset_fields = ('area', 'region', 'settlement_type')
    queryset = Settlement.objects.all()
    serializer_class = SettlementSerializer


class CityListAPIView(BaseNovaposhtaListAPIView):
    filterset_fields = ('area', 'settlement_type')
    queryset = City.objects.all()
    serializer_class = CitySerializer


class StreetListAPIView(BaseNovaposhtaListAPIView):
    queryset = Street.objects.all()
    serializer_class = StreetSerializer
    filterset_class = StreetsFilterSet


class WarehouseListAPIView(BaseNovaposhtaListAPIView):
    queryset = Warehouse.objects.select_related('type_of_warehouse')
    serializer_class = WarehouseSerializer
    search_fields = (
        "description",
        "description_ru",
        "type_of_warehouse__ref",
        "type_of_warehouse__description",
        "city__description",
        "city__description_ru",
    )
    filterset_fields = (
        "type_of_warehouse",
        "city_ref",
        "city",
        "settlement_ref"
    )


class WarehouseDetailAPIView(StandardAPIViewMixin, RetrieveAPIView):
    queryset = Warehouse.objects.all()
    serializer_class = WarehouseDetailSerializer
