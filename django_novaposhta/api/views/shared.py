from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from rest_framework import status

from ...getters import active_user_preferences_receiver, get_preferences

__all__ = (
    'ActionGenericAPIView',
    'NovaposhtaPreferencesGetterViewMixin',
)


class ActionGenericAPIView(GenericAPIView):

    def perform_action(self, serializer):
        raise NotImplementedError

    def get_response(self, result, serializer):
        return serializer.data

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        try:
            result = self.perform_action(serializer)
        except Exception as e:
            raise e
            # raise serializers.ValidationError(str(e))

        return Response(
            data=self.get_response(result, serializer),
            status=status.HTTP_200_OK
        )


class NovaposhtaPreferencesGetterViewMixin:

    def get_preferences(self):
        return get_preferences()
