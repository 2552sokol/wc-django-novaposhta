from rest_framework import serializers

from django_novaposhta.exceptions import NovaposhtaValidationError
from django_novaposhta.models import (
    Document,
    ScanSheet
)
from django_novaposhta.validators import validate_documents_scan_sheet_allowed

__all__ = (
    'NovaposhtaScanSheetRetrieveSerializer',
    'ScanSheetDocumentsSerializer',
    'InsertDocumentsSerializer',
)


class NovaposhtaScanSheetRetrieveSerializer(serializers.ModelSerializer):

    class Meta:
        model = ScanSheet
        fields = (
            'id',
            'ref',
            'date',
            'printed',
            'count',
            'sender',
            'pdf',
            'marking_pdf',
            'is_dispatched',
            'created_at',
            'updated_at',
        )


class ScanSheetDocumentsSerializer(serializers.Serializer):
    documents = serializers.PrimaryKeyRelatedField(
        queryset=Document.objects.all(), many=True
    )


class InsertDocumentsSerializer(ScanSheetDocumentsSerializer):
    scan_sheet = serializers.PrimaryKeyRelatedField(
        queryset=ScanSheet.objects.all(), required=False
    )

    def validate_documents(self, documents):
        try:
            validate_documents_scan_sheet_allowed(documents)
        except NovaposhtaValidationError as e:
            raise serializers.ValidationError(str(e))
        return documents
