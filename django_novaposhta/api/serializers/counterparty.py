from rest_framework import serializers


from .common_entities import (
    BaseNovaposhtaSerializer,
    AddressSerializer
)
from ...models import (
    Counterparty,
    Street
)
from ...services import (
    create_counterparty,
)
from ...services.addresses import create_counterparty_address

__all__ = (
    'CounterpartySerializer',
    'CounterpartyCreateSerializer',
)


class CounterpartySerializer(BaseNovaposhtaSerializer):
    address = AddressSerializer(write_only=True)

    class Meta:
        model = Counterparty
        fields = '__all__'


class CounterpartyCreateSerializer(BaseNovaposhtaSerializer):
    address_fields = [
        'street',
        'building_number',
        'flat',
        'note'
    ]

    street = serializers.PrimaryKeyRelatedField(
        queryset=Street.objects.all(),
        write_only=True
    )
    building_number = serializers.CharField(write_only=True)
    flat = serializers.CharField(write_only=True)
    note = serializers.CharField(write_only=True)

    class Meta:
        model = Counterparty
        fields = [
            'first_name',
            'middle_name',
            'last_name',
            'phone',
            'email',
            'counterparty_type',
            'counterparty_property',
            'street',
            'building_number',
            'flat',
            'note'
        ]

    def create(self, validated_data):
        address_data = {
            field: validated_data.pop(field, None)
            for field in self.address_fields
        }

        validated_data['user'] = self.context['request'].user

        counterparties = create_counterparty(
            **validated_data
        )

        for counterparty in counterparties:
            create_counterparty_address(
                counterparty=counterparty,
                **address_data
            )

        return counterparties[0]