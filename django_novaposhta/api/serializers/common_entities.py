from typing import TYPE_CHECKING

from django.utils.translation import get_language

from rest_framework import serializers

from .mixins import (
    DynamicFieldsSerializerMixin,
    RequestSerializerMixin,
)
from ...models import (
    Address,
    Area,
    CargoType,
    City,
    Counterparty,
    CounterpartyType,
    PayerType,
    PaymentForm,
    Region,
    ServiceType,
    Settlement,
    SettlementType,
    Street,
    Warehouse,
    WarehouseType
)

if TYPE_CHECKING:
    from ...models.abstracts import BaseNovaposhtaModel

__all__ = (
    'BaseNovaposhtaSerializer',
    'CargoTypeSerializer',
    "CounterpartyTypeSerializer",
    "PayerTypeSerializer",
    "PaymentFormSerializer",
    "ServiceTypeSerializer",
    "WarehouseTypeSerializer",
    'AreaSerializer',
    "RegionSerializer",
    "SettlementTypeSerializer",
    "SettlementSerializer",
    "CitySerializer",
    "StreetSerializer",
    "WarehouseSerializer",
    "WarehouseDetailSerializer",
    "AddressSerializer",
)


class BaseNovaposhtaSerializer(
    DynamicFieldsSerializerMixin,
    RequestSerializerMixin,
    serializers.ModelSerializer
):
    description = serializers.SerializerMethodField()

    def get_description(self, obj: 'BaseNovaposhtaModel'):
        """
        Helper method field to return active translation for description field
        """
        language = get_language()

        if language == 'ru':
            return getattr(obj, 'description_ru', obj.description)

        return obj.description


class CargoTypeSerializer(BaseNovaposhtaSerializer):
    class Meta:
        model = CargoType
        fields = "__all__"


class CounterpartyTypeSerializer(BaseNovaposhtaSerializer):
    class Meta:
        model = CounterpartyType
        fields = "__all__"


class PayerTypeSerializer(BaseNovaposhtaSerializer):
    class Meta:
        model = PayerType
        fields = "__all__"


class PaymentFormSerializer(BaseNovaposhtaSerializer):
    class Meta:
        model = PaymentForm
        fields = "__all__"


class ServiceTypeSerializer(BaseNovaposhtaSerializer):
    class Meta:
        model = ServiceType
        fields = "__all__"


class WarehouseTypeSerializer(BaseNovaposhtaSerializer):
    class Meta:
        model = WarehouseType
        fields = "__all__"


class AreaSerializer(BaseNovaposhtaSerializer):
    class Meta:
        ref_name = 'django_novaposhta_area'
        model = Area
        fields = "__all__"


class RegionSerializer(BaseNovaposhtaSerializer):
    class Meta:
        model = Region
        fields = "__all__"


class SettlementTypeSerializer(BaseNovaposhtaSerializer):
    class Meta:
        model = SettlementType
        fields = "__all__"


class SettlementSerializer(BaseNovaposhtaSerializer):
    class Meta:
        ref_name = 'django_novaposhta_settlement'
        model = Settlement
        fields = "__all__"


class CitySerializer(BaseNovaposhtaSerializer):
    class Meta:
        ref_name = 'django_novaposhta_city'
        model = City
        fields = "__all__"


class StreetSerializer(BaseNovaposhtaSerializer):
    class Meta:
        model = Street
        fields = '__all__'


class WarehouseSerializer(BaseNovaposhtaSerializer):
    type_of_warehouse = WarehouseTypeSerializer()

    class Meta:
        model = Warehouse
        fields = (
            "id",
            "ref",
            "number",
            "city_ref",
            "type_of_warehouse",
            "description",
            "description_ru",
            "longitude",
            "latitude",
        )

class WarehouseDetailSerializer(BaseNovaposhtaSerializer):
    city = CitySerializer()
    type_of_warehouse = WarehouseTypeSerializer()

    class Meta:
        model = Warehouse
        fields = "__all__"


class AddressSerializer(BaseNovaposhtaSerializer):
    class Meta:
        model = Address
        fields = "__all__"



