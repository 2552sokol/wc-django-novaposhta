from decimal import Decimal

from django.core.validators import MinValueValidator
from rest_framework import serializers

from django_novaposhta.models import Document

__all__ = (
    'OptionSeatCreateSerializer',
    'DocumentRetrieveSerializer',
    'NovaposhtaDocumentPrintMarkingSerializer',
)


VOLUME_CONVERT_VALUE = '0.000001'


class OptionSeatCreateSerializer(serializers.Serializer):
    weight = serializers.DecimalField(
        validators=(MinValueValidator(0),),
        max_digits=10, decimal_places=1
    )
    volumetricWidth = serializers.DecimalField(
        validators=(MinValueValidator(0),),
        max_digits=10, decimal_places=1
    )
    volumetricLength = serializers.DecimalField(
        validators=(MinValueValidator(0),),
        max_digits=10, decimal_places=1
    )
    volumetricHeight = serializers.DecimalField(
        validators=(MinValueValidator(0),),
        max_digits=10, decimal_places=1
    )

    def to_internal_value(self, data):
        data = super().to_internal_value(data)
        volumetric_volume = (
            data['volumetricWidth'] * data['volumetricLength'] *
            data['volumetricHeight'] * Decimal(VOLUME_CONVERT_VALUE)
        )
        data['volumetricVolume'] = volumetric_volume.quantize(Decimal('0.001'))
        return data


class DocumentRetrieveSerializer(serializers.ModelSerializer):

    class Meta:
        model = Document
        fields = (
            'id',
            'int_doc_number',
            'cost_on_site',
            'type_document',
            'estimated_delivery_date',
            'status',
            'verbose_status',
            'pdf',
            'marking_pdf',
        )


class NovaposhtaDocumentPrintMarkingSerializer(serializers.Serializer):
    is_zebra_printer = serializers.BooleanField(default=False)
