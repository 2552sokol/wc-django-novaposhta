from django.urls import path, include

from .views import (
    CargoTypeListAPIView,
    CounterpartyTypeListAPIView,
    PayerTypeListAPIView,
    PaymentFormListAPIView,
    ServiceTypeListAPIView,
    AreaListAPIView,
    RegionListAPIView,
    SettlementTypeListAPIView,
    SettlementListAPIView,
    CityListAPIView,
    StreetListAPIView,
    WarehouseTypeListAPIView,
    WarehouseListAPIView,
    WarehouseDetailAPIView,

    # CounterpartyCreateAPIView,
)

app_name = 'novaposhta-api'

urlpatterns = [
    path('novaposhta/', include([
        path('address/', include(([
            path('areas/', AreaListAPIView.as_view(), name='areas'),
            path('cities/', CityListAPIView.as_view(), name='cities'),
            path('regions/', RegionListAPIView.as_view(), name='regions'),
            path('settlements/', SettlementListAPIView.as_view(), name='settlements'),
            path('settlement-types/', SettlementTypeListAPIView.as_view(), name='settlement-types'),
            path('streets/', StreetListAPIView.as_view(), name='streets'),
            path('warehouse-types/', WarehouseTypeListAPIView.as_view(), name='warehouse-types'),
            path('warehouses/', WarehouseListAPIView.as_view(), name='warehouses'),
            path('warehouse/<int:pk>/retrieve/', WarehouseDetailAPIView.as_view(), name='warehouse'),
        ], 'address'))),
        path('common/', include(([
            path('cargo-types/', CargoTypeListAPIView.as_view(), name='cargo-types'),
            path('counterparty-types/', CounterpartyTypeListAPIView.as_view(), name='counterparty-types'),
            path('payer-types/', PayerTypeListAPIView.as_view(), name='payer-types'),
            path('payment-forms/', PaymentFormListAPIView.as_view(), name='payment-forms'),
            path('service-types/', ServiceTypeListAPIView.as_view(), name='service-types'),
        ], 'common'))),
        # path('counterparty/', include(([
        #     path('create/', CounterpartyCreateAPIView.as_view(), name='create')
        # ], 'counterparty'))),
    ]))
]
