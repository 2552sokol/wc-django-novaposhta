from django.conf import settings
from rest_framework.throttling import UserRateThrottle

from ..consts import NOVAPOSHTA_REQUEST_THROTTLING_DEFAULT_TIME

__all__ = (
    'NovaposhtaUserRateThrottle',
)


NOVAPOSHTA_REQUEST_THROTTLING_TIME = getattr(
    settings,
    'NOVAPOSHTA_REQUEST_THROTTLING_TIME',
    NOVAPOSHTA_REQUEST_THROTTLING_DEFAULT_TIME
)


class NovaposhtaUserRateThrottle(UserRateThrottle):
    scope = 'novaposhta'

    def wait(self):
        return NOVAPOSHTA_REQUEST_THROTTLING_TIME
