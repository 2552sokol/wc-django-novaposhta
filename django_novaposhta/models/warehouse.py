from django.contrib.postgres.fields import JSONField
from django.db import models
from django.utils.translation import pgettext_lazy

from .abstracts import BaseNovaposhtaModel

__all__ = (
    "Warehouse",
)


class Warehouse(BaseNovaposhtaModel):
    ref = models.CharField(
        pgettext_lazy('novaposhta', "Ref"),
        max_length=99,
        unique=True
    )
    site_key = models.CharField(
        pgettext_lazy('novaposhta', "Site key"),
        max_length=50
    )
    description_ru = models.CharField(
        pgettext_lazy('novaposhta', "Description in russian"),
        max_length=99,
        db_index=True
    )
    type_of_warehouse = models.ForeignKey(
        'django_novaposhta.WarehouseType',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='warehouses',
        verbose_name=pgettext_lazy('novaposhta', "Type of warehouse"),
    )
    city_ref = models.CharField(
        pgettext_lazy('novaposhta', "City ref"),
        max_length=37,
        blank=True,
    )
    settlement_ref = models.CharField(
        pgettext_lazy('novaposhta', "Settlement ref"),
        max_length=37,
        blank=True,
    )
    city = models.ForeignKey(
        'django_novaposhta.City',
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name='warehouses',
        verbose_name=pgettext_lazy('novaposhta', "City"),
    )
    phone = models.CharField(
        pgettext_lazy('novaposhta', "Phone"),
        max_length=99,
        blank=True
    )
    longitude = models.CharField(
        pgettext_lazy('novaposhta', "Longitude"),
        max_length=50
    )
    latitude = models.CharField(
        pgettext_lazy('novaposhta', "Latitude"),
        max_length=50
    )
    post_finance = models.PositiveSmallIntegerField(
        pgettext_lazy('novaposhta', "Post finance (boolean)"),
        default=0
    )
    post_terminal = models.PositiveSmallIntegerField(
        pgettext_lazy('novaposhta', "Post terminal (boolean)"),
        default=0
    )
    number = models.CharField(
        pgettext_lazy('novaposhta', "Number"),
        max_length=255
    )
    reception = JSONField(
        pgettext_lazy('novaposhta', "Reception")
    )
    delivery = JSONField(
        pgettext_lazy('novaposhta', "Delivery")
    )
    schedule = JSONField(
        pgettext_lazy('novaposhta', "Schedule")
    )

    class Meta(BaseNovaposhtaModel.Meta):
        verbose_name = pgettext_lazy('novaposhta', "Warehouse")
        verbose_name_plural = pgettext_lazy('novaposhta', "Warehouses")
