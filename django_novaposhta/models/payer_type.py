from django.utils.translation import pgettext_lazy

from .abstracts import BaseNovaposhtaModel

__all__ = (
    "PayerType",
)


class PayerType(BaseNovaposhtaModel):
    class Meta(BaseNovaposhtaModel.Meta):
        verbose_name = pgettext_lazy('novaposhta', "Payer type")
        verbose_name_plural = pgettext_lazy('novaposhta', "Payer types")
