from django.utils.translation import pgettext_lazy

from .abstracts import BaseNovaposhtaModel

__all__ = (
    "ServiceType",
)


class ServiceType(BaseNovaposhtaModel):
    class Meta(BaseNovaposhtaModel.Meta):
        verbose_name = pgettext_lazy('novaposhta', "Service type")
        verbose_name_plural = pgettext_lazy('novaposhta', "Service types")
