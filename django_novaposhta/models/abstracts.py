from django.contrib.postgres.fields import JSONField
from django.contrib.postgres.indexes import BTreeIndex
from django.db import models
from django.utils.translation import pgettext_lazy

from .mixins import ContentTypeGetterMixin

__all__ = (
    'BaseNovaposhtaModel',
    'AutocompleteSearchFieldMixin',
    'BasePerson',
)


class BaseNovaposhtaModel(ContentTypeGetterMixin, models.Model):
    ref = models.CharField(
        pgettext_lazy('novaposhta', "Ref"),
        max_length=50,
        db_index=True
    )
    description = models.CharField(
        pgettext_lazy('novaposhta', "Description"),
        max_length=255,
        db_index=True
    )
    created_at = models.DateTimeField(
        pgettext_lazy('novaposhta', 'Created at'),
        auto_now_add=True,
    )
    updated_at = models.DateTimeField(
        pgettext_lazy('novaposhta', 'Updated at'),
        auto_now=True
    )
    raw_data = JSONField(blank=True, default=dict)

    class Meta:
        abstract = True
        indexes = (
            BTreeIndex(fields=['created_at']),
        )
        ordering = ['created_at']

    def __str__(self) -> str:
        return self.description


class AutocompleteSearchFieldMixin:

    @staticmethod
    def autocomplete_search_fields():
        return 'description',


class BasePerson(models.Model):
    first_name = models.CharField(
        pgettext_lazy('novaposhta', "First name"),
        max_length=255,
        blank=True
    )
    middle_name = models.CharField(
        pgettext_lazy('novaposhta', "Middle name"),
        max_length=255,
        blank=True
    )
    last_name = models.CharField(
        pgettext_lazy('novaposhta', "Last name"),
        max_length=255,
        blank=True
    )
    email = models.CharField(
        pgettext_lazy('novaposhta', "Email"),
        max_length=255,
        blank=True
    )

    class Meta:
        abstract = True
