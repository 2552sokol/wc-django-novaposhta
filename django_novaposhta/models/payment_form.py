from django.utils.translation import pgettext_lazy

from .abstracts import BaseNovaposhtaModel

__all__ = (
    "PaymentForm",
)


class PaymentForm(BaseNovaposhtaModel):
    class Meta(BaseNovaposhtaModel.Meta):
        verbose_name = pgettext_lazy('novaposhta', "Payment form")
        verbose_name_plural = pgettext_lazy('novaposhta', "Payment forms")
