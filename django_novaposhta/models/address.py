from django.db import models
from django.utils.translation import pgettext_lazy

from .abstracts import BaseNovaposhtaModel, AutocompleteSearchFieldMixin

__all__ = (
    "Address",
)


class Address(
    AutocompleteSearchFieldMixin,
    BaseNovaposhtaModel
):
    description_ru = models.CharField(
        pgettext_lazy('novaposhta', "Description in russian"),
        max_length=255,
        blank=True
    )
    counterparty = models.ForeignKey(
        'django_novaposhta.Counterparty',
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name='addresses',
        verbose_name=pgettext_lazy('novaposhta', "Counterparty")
    )
    street = models.ForeignKey(
        'django_novaposhta.Street',
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name='addresses',
        verbose_name=pgettext_lazy('novaposhta', "Street")
    )

    class Meta(BaseNovaposhtaModel.Meta):
        verbose_name = pgettext_lazy('novaposhta', "Address")
        verbose_name_plural = pgettext_lazy('novaposhta', "Addresses")
