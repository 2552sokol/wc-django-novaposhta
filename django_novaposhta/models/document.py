from django.contrib.postgres.fields import JSONField
from django.db import models
from django.utils.translation import pgettext_lazy

from .abstracts import BaseNovaposhtaModel, AutocompleteSearchFieldMixin
from ..consts import (
    DOCUMENT_TRACKING_DEFAULT_STATUS,
    DOCUMENT_TRACKING_STATUSES,
    DOCUMENT_TRACKING_DEFAULT_VERBOSE_STATUS
)
from ..file_storage import novaposhta_upload_storage
from ..utils import file_upload_to

__all__ = (
    "Document",
)


class Document(
    AutocompleteSearchFieldMixin,
    BaseNovaposhtaModel
):
    description = None
    int_doc_number = models.CharField(
        pgettext_lazy('novaposhta', "Number"),
        max_length=36
    )
    cost_on_site = models.CharField(
        pgettext_lazy('novaposhta', "Cost on site"),
        max_length=36
    )
    type_document = models.CharField(
        pgettext_lazy('novaposhta', "Document Type"),
        max_length=36
    )
    estimated_delivery_date = models.CharField(
        pgettext_lazy('novaposhta', "Estimated delivery date"),
        max_length=255
    )
    request_data = JSONField(
        pgettext_lazy('novaposhta', "Request data"),
        blank=True,
        default=dict
    )
    status = models.CharField(
        pgettext_lazy('novaposhta', "Status"),
        default=DOCUMENT_TRACKING_DEFAULT_STATUS,
        choices=DOCUMENT_TRACKING_STATUSES,
        max_length=3,
    )
    verbose_status = models.CharField(
        pgettext_lazy('novaposhta', "Verbose status"),
        blank=True, max_length=255,
        default=DOCUMENT_TRACKING_DEFAULT_VERBOSE_STATUS
    )
    is_tracking = models.BooleanField(
        pgettext_lazy('novaposhta', "Is tracking"),
        default=True
    )
    scan_sheet = models.ForeignKey(
        'django_novaposhta.ScanSheet',
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name='documents',
        verbose_name=pgettext_lazy('novaposhta', "Scan Sheet"),
    )
    pdf = models.FileField(
        pgettext_lazy('novaposhta', "PDF"),
        null=True,
        blank=True,
        upload_to=file_upload_to,
        storage=novaposhta_upload_storage,
    )
    marking_pdf = models.FileField(
        pgettext_lazy('novaposhta', "Marking PDF"),
        null=True,
        blank=True,
        upload_to=file_upload_to,
        storage=novaposhta_upload_storage,
    )

    class Meta(BaseNovaposhtaModel.Meta):
        verbose_name = pgettext_lazy('novaposhta', "Document")
        verbose_name_plural = pgettext_lazy('novaposhta', "Documents")

    def __str__(self) -> str:
        return self.int_doc_number
