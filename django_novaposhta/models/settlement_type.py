from django.db import models
from django.utils.translation import pgettext_lazy

from .abstracts import BaseNovaposhtaModel

__all__ = (
    "SettlementType",
)


class SettlementType(BaseNovaposhtaModel):
    description_ru = models.CharField(
        pgettext_lazy('novaposhta', "Description in russian"),
        max_length=50,
        db_index=True
    )

    class Meta(BaseNovaposhtaModel.Meta):
        verbose_name = pgettext_lazy('novaposhta', "Settlement type")
        verbose_name_plural = pgettext_lazy('novaposhta', "Settlement types")
