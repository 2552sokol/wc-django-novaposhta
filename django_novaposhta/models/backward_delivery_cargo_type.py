from django.utils.translation import pgettext_lazy

from .abstracts import BaseNovaposhtaModel

__all__ = (
    "BackwardDeliveryCargoType",
)


class BackwardDeliveryCargoType(BaseNovaposhtaModel):
    class Meta(BaseNovaposhtaModel.Meta):
        verbose_name = pgettext_lazy('novaposhta', "Backward Delivery Cargo type")
        verbose_name_plural = pgettext_lazy('novaposhta', "Backward Delivery Cargo types")
